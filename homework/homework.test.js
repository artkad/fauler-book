const { expect, it } = require("@jest/globals");
const {Province, sampleProvinceData} = require("./index.js");

describe("province", function() {
  it("shortfall", function() {
    const asia = new Province(sampleProvinceData());
    expect(asia.shortfall).toBe(5);
  })

  it("profit", function() {
    const asia = new Province(sampleProvinceData());
    expect(asia.profit).toBe(230);
  });
});
