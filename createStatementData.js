//plays.json...
const plays =
{
  hamlet: { name: "Hamlet", type: "tragedy" },
  aslike: { name: "As You Like It", type: "comedy" },
  othello: { name: "Othello", type: "tragedy" }
};

//invoices.json...
const invoice = [
  {
    customer: "BigCo",
    performances: [
      {
        playID: "hamlet",
        audience: 55
      },
      {
        playID: "aslike",
        audience: 35
      },
      {
        playID: "othello",
        audience: 40
      }
    ]
  }
];

function createStatementData(invoice, plays) {
  const result = {};

  result.customer = invoice[0].customer;
  result.performances = invoice[0].performances.map(enrichPerformance);
  result.totalAmount = totalAmount(result);
  result.totalVolumeCredits = totalVolumeCredits(result);
  return result;
}

class PerformanceCalculator {
  constructor(aPerformance, aPlay) {
    this.performance = aPerformance;
    this.play = aPlay;
  }
  // console.log(this);
  get amount() {
    throw new Error("subclass resposibility");
  }

  get volumeCredits() {
    return Math.max(this.performance.audience - 30, 0);
  }
}

function createPerformanceCalculator(aPerformance, aPlay) {
  switch (aPlay.type) {
    case "tragedy": return new TragedyCalculator(aPerformance, aPlay);
    case "comedy": return new ComedyCalculator(aPerformance, aPlay);
    default: throw new Error(`unknown type: ${aPlay.type}`);
  }
}

class TragedyCalculator extends PerformanceCalculator {
  get amount() {
    let result = 40000;
    if (this.performance.audience > 30) {
      result += 1000 * (this.performance.audience - 30);
    }
    return result;
  }
}
class ComedyCalculator extends PerformanceCalculator {
  get amount() {
    let result = 30000;
    if (this.performance.audience > 20) {
      result += 10000 + 500 * (this.performance.audience - 20);
    }
    result += 300 * this.performance.audience;
    return result;
  }

  get volumeCredits() {
    return super.volumeCredits + Math.floor(this.performance.audience / 5);
  }
}

function enrichPerformance(aPerformance) {
  const calculator = createPerformanceCalculator(aPerformance, playFor(aPerformance));
  const result = Object.assign({}, aPerformance);
  result.play = calculator.play;
  result.amount = calculator.amount;
  result.volumeCredits = calculator.volumeCredits;
  // console.log(result);

  return result;
}

function playFor(aPerformance) {
  return plays[aPerformance.playID];
}
// ??
// function amountFor(aPerformance) {
//   return new PerformanceCalculator(aPerformance, playFor(aPerformance)).amount;
// }

// function volumeCreditsFor(aPerformance) {
//   let volumecredits = 0;
//   volumecredits += Math.max(aPerformance.audience - 30, 0);
//   if ("comedy" === aPerformance.play.type)
//     volumecredits += Math.floor(aPerformance.audience / 5);
//   return volumecredits;
// }

function totalAmount(data) {
  let result = 0;
  for (let perf of data.performances) {
    result += perf.amount;
  }
  return data.performances.reduce((total, p) => total + p.amount, 0);
}

function totalVolumeCredits(data) {
  return data.performances.reduce((total, p) => total + p.volumeCredits, 0);
}

module.exports = createStatementData;