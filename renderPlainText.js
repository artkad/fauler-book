//plays.json...
const plays =
{
  hamlet: { name: "Hamlet", type: "tragedy" },
  aslike: { name: "As You Like It", type: "comedy" },
  othello: { name: "Othello", type: "tragedy" }
}

//invoices.json...
const invoice = [
  {
    customer: "BigCo",
    performances: [
      {
        playID: "hamlet",
        audience: 55
      },
      {
        playID: "aslike",
        audience: 35
      },
      {
        playID: "othello",
        audience: 40
      }
    ]
  }]

function renderPlainText(data) {
  let result = `Statement for ${data.customer}\n`;

  for (let perf of data.performances) {
   
    // Вывод строки счета
    result += `${perf.play.name}:`;
    result += ` ${usd(perf.amount)}(${perf.audience} seats)\n`;
  }

  result += `Amount owed is ${usd(data.totalAmount)}\n`;
  result += `You earned ${data.totalVolumeCredits} credits`;
  return result;
}

function usd(aNumber) {
  return new Intl.NumberFormat("en-US",
    {
      style: "currency", currency: "USD",
      minimumFractionDigits: 2
    }).format(aNumber / 100);
}

module.exports = renderPlainText

